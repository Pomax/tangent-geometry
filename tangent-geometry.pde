/*
  https://math.stackexchange.com/questions/2086641/find-the-center-of-a-circle-tangent-to-two-circles-with-a-known-radius
 http://jwilson.coe.uga.edu/EMAT6680Su06/Swanagan/Assignment7/BSAssignment7.html
 */

int dim;
ArrayList<Circle> circles;
boolean moving = false;
float s = 0.01;


void setup() {
  size(1500, 1000); // I still don't understand why I can't feed this `final int` or even `3 * 500`. It's ridiculous.
  dim = width/3;
  reset();
}


void reset() {
  circles = new ArrayList<Circle>();
  noLoop();
  ellipseMode(CENTER);
  textAlign(CENTER, CENTER);

  for (int i=0; i<2; i++) {
    float r = random(20, 120);
    circles.add(new Circle(random(r, dim-r), random(r, dim-r), r));
  }

  if (circles.get(0).r < circles.get(1).r) {
    Circle _ = circles.get(0);
    circles.set(0, circles.get(1));
    circles.set(1, _);
  }
}


void keyPressed() {
  if (key == ' ') { 
    reset();
  }
  if (keyCode == 38) { 
    s += 0.01;
  }
  if (keyCode == 40) { 
    s -= 0.01;
  }
  redraw();
}


void draw() {
  noFill();
  stroke(0);
  background(245);
  for (Circle c : circles) {  
    c.draw();
  }

  Circle c1 = circles.get(0);
  Circle c2 = circles.get(1);
  c1.calculateTangents(c2);
  line(c1, c2);
  drawConstruction(c1, c2);
  drawShapes(c1, c2);

  // let's goof around
  tryArcJoin(c1, c2);
}


void tryArcJoin(Circle c1, Circle c2) {
  resetMatrix();
  stroke(0);
  noFill();
  
  Circle c4 = new Circle(
    c1.x + c1.r * cos(c1.ex.phi1 - s), 
    c1.y + c1.r * sin(c1.ex.phi1 - s), 
    c2.r
    );

  c4.draw();

  Point p = new Point(
    lerp(c1.x, c4.x, (c1.r-c2.r)/c1.r), 
    lerp(c1.y, c4.y, (c1.r-c2.r)/c1.r)
    );

  p.draw();

  line(c1, c4);
  line(p, c2);

  Point p2 = new Point(
    lerp(p.x, c2.x, 0.5), 
    lerp(p.y, c2.y, 0.5)
    );

  p2.draw();

  Point n = new Point( 
    p2.x - (c2.y - p2.y), // originx + y
    p2.y + (c2.x - p2.x) // originy - x
    );

  line(p2, n);

  Point hc = intersection(c1, c4, p2, n);

  if (hc != null) {
    Circle H = new Circle(hc, dist(hc, c4));
    H.draw();
    ((Point)H).draw();
    stroke(0, 40);
    fill(200, 0, 0);
    Point p4 = lerp(c2, hc, c2.r/dist(c2, hc));
    p4.draw();

    // get angles
    float phi1 = atan2(c4.y - H.y, c4.x - H.x);
    float phi2 = atan2(p4.y - H.y, p4.x - H.x);
    if (phi2<phi1) phi2 += TAU;
    fill(200, 0, 200, 20);
    arc(H.x, H.y, H.d, H.d, phi1, phi2);
    println(phi1, phi2, H.r);

    line(c2, H);
  }
}


void drawShapes(Circle c1, Circle c2) {
  resetMatrix();
  translate(0, dim);
  line(0, 0, width, 0);

  noStroke();
  fill(0, 0, 200);
  c1.draw();
  c2.draw();

  translate(dim, 0);

  stroke(0);
  line(0, 0, 0, dim);

  c1.drawExtangents(c2);

  translate(dim, 0);

  stroke(0);
  line(0, 0, 0, dim);

  c1.drawIntangents(c2);
}



void drawPoly(Point ...pts) {
  beginShape();
  for (Point p : pts) {
    vertex(p.x, p.y);
    ellipse(p.x, p.y, 3, 3);
  }  
  endShape(CLOSE);
}


void mouseClicked() {
  moving = !moving;
}


void mouseMoved() {
  if (moving) {
    circles.get(0).x = mouseX;
    circles.get(0).y = mouseY;
    redraw();
  }
}
