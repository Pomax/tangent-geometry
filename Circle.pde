/**
 * circle class with a center, radius, and diameter
 */
class Circle extends Point {
  float r, d;
  TangentData ex, in;

  public Circle(Point p, float r) {
    this(p.x, p.y, r);
  }
  
  public Circle(float x, float y, float r) {
    super(x, y);
    this.r = r;
    this.d = 2 * r;
  }
  
  void calculateTangents(Circle c2) {
    float dx = c2.x - x, dy = c2.y - y;
    ex = computeExtangents(this, c2, dx, dy);
    in = computeIntangents(this, c2, dx, dy);
  }

  void draw() {
    ellipse(x, y, d, d);
  }

  void drawExtangents(Circle c2) {
    if (ex == null) this.calculateTangents(c2);
    this.drawExtangentShape(c2, ex);
  }

  void drawIntangents(Circle c2) {
    if (in == null) this.calculateTangents(c2);
    this.drawIntangentShape(c2, in);
  }

  void drawExtangentShape(Circle c2, TangentData d) {
    Circle c1 = this;
    float a1 = d.phi1, a2 = d.phi2, a;

    stroke(0, 0, 200);
    fill(0, 0, 200);
    
    beginShape();
    if (a1 > a2) a1 -= TAU;
    for (a = a1; a < a2; a += 0.01) {
      vertex(c1.x + c1.r * cos(a), c1.y + c1.r * sin(a));
    }
    if (a2 > a1) a2 -= TAU;
    for (a = a2; a < a1; a += 0.01) {
      vertex(c2.x + c2.r * cos(a), c2.y + c2.r * sin(a));
    }
    endShape(CLOSE);
  }

  void drawIntangentShape(Circle c2, TangentData d) {
    Circle c1 = this;
    float a1 = d.phi1, a2 = d.phi2, a;

    stroke(0, 0, 200);
    fill(0, 0, 200);

    beginShape();
    if (a2 > a1) a2 -= TAU;
    for (a = a2; a < a1; a += 0.01) {
      vertex(c1.x + c1.r * cos(a), c1.y + c1.r * sin(a));
    }
    if (a2 > a1) a1 += TAU;
    for (a = a1; a > a2; a -= 0.01) {
      vertex(c2.x + c2.r * cos(a + PI), c2.y + c2.r * sin(a + PI));
    }
    endShape(CLOSE);
  }
}
