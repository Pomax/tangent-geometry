/**
 * tangent data because java has no structs
 */
class TangentData {
  Circle c1, c2;
  Point t1, t2, t3, s1, s2;
  float A, H, phi1, phi2, da;

  public TangentData(Circle _c1, Circle _c2, float _A, float _H, float _phi1, float _phi2, float _da, Point ...points) {
    // the circles involved
    c1 = _c1;
    c2 = _c2;
    
    // our basic geometry values
    A = _A;
    H = _H;
    phi1 = _phi1;
    phi2 = _phi2;
    da = _da;

    // third triangle point for visualising construction.
    t3 = points[2];

    // tangent points on one side.
    t1 = points[0];
    s1 = points[3];

    // tangent points on the other side.
    t2 = points[1];
    s2 = points[4];
    
  }
}
