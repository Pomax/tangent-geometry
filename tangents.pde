// Convert a polar coordinate to Euclidean coordinate.
Point fromPolar(Point origin, float d, float phi) {
  return new Point(
    origin.x + d * cos(phi),
    origin.y + d * sin(phi)
  );
}


TangentData computeExtangents(Circle c1, Circle c2, float dx, float dy) {
  float A = abs(c1.r - c2.r);
  float H = dist(c1.x, c1.y, c2.x, c2.y);
  float p = acos(A/H);
  float da = atan2(dy, dx);

  // we can now calculate our r1, r2, and r3 points:
  float phi1 = da + p;
  Point t3 = fromPolar(c1, A, phi1);
  Point t2 = fromPolar(c2, c2.r, phi1);
  Point t1 = fromPolar(c1, c1.r, phi1);

  // and our reflections:
  float phi2 = da - p;
  Point s2 = fromPolar(c2, c2.r, phi2);
  Point s1 = fromPolar(c1, c1.r, phi2);
  
  return new TangentData(c1, c2, A, H, phi1, phi2, da, t1, t2, t3, s1, s2);
}


TangentData computeIntangents(Circle c1, Circle c2, float dx, float dy) {
  float A = abs(c1.r + c2.r);  
  float H = sqrt(dx*dx + dy*dy);
  float phi = asin(A/H);
  float da = atan2(dy, dx);

  // we can now calculate our r1, r2, and r3 points:
  float phi1 = da + phi - PI/2;
  Point t1 = fromPolar(c1, c1.r, phi1);
  Point t2 = fromPolar(c2, c2.r, phi1 + PI);
  Point t3 = fromPolar(c2, A, phi1 + PI);

  // and our reflections:
  float phi2 = da - phi + PI/2;
  Point s1 = fromPolar(c1, c1.r, phi2);
  Point s2 = fromPolar(c2, c2.r, phi2 + PI);

  return new TangentData(c1, c2, A, H, phi1, phi2, da, t1, t2, t3, s1, s2);
}
