/**
 * simple (x,y) point class
 */
class Point {
  float x, y;

  public Point(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  void draw() {
    ellipse(x,y,3,3);
  }
}
