Point intersection(Point p1, Point p2, Point p3, Point p4) {
  return intersection(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y);
}

Point intersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
  float nx = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4), 
    ny = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4), 
    d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
  if (d == 0) return null;
  return new Point(nx / d, ny / d );
}
