Point lerp(Point p1, Point p2, float ratio) {
  return new Point(
    lerp(p1.x, p2.x, ratio), 
    lerp(p1.y, p2.y, ratio)
    );
}

float dist(Point p1, Point p2) {
  return dist(p1.x, p1.y, p2.x, p2.y);
}

void line(Point p1, Point p2) {
  line(p1.x, p1.y, p2.x, p2.y);
}

void drawConstruction(Circle c1, Circle c2) {
  float dy = c2.y - c1.y;
  float dx = c2.x - c1.x;

  resetMatrix();

  noFill();
  stroke(0);
  ellipse(c1.x, c1.y, 3, 3); 
  ellipse(c2.x, c2.y, 3, 3); 
  ellipse(c1.x, c1.y, c1.d, c1.d); 


  translate(dim, 0);

  stroke(0);
  fill(0);
  noFill();
  text("outer tangents", 55, 15);
  drawBasics(c1, c2, dx, dy);
  drawExtangents(c1, c2, dx, dy);

  translate(dim, 0);

  stroke(0);
  fill(0);
  noFill();
  text("inner tangents", 55, 15);
  drawBasics(c1, c2, dx, dy);
  drawIntangents(c1, c2, dx, dy);
}


void drawBasics(Circle c1, Circle c2, float dx, float dy) {
  line(0, 0, 0, dim);
  c1.draw();
  c2.draw();
}


void drawExtangents(Circle c1, Circle c2, float dx, float dy) {
  TangentData r = c1.ex;
  drawExtangentData(r.A, r.phi1, r.da, c1, c2, r.t1, r.t2, r.t3, r.s1, r.s2);
}


void drawIntangents(Circle c1, Circle c2, float dx, float dy) {
  TangentData r = c1.in;
  drawIntangentData(r.A, r.phi1, r.da, c1, c2, r.t1, r.t2, r.t3, r.s1, r.s2);
}


void drawExtangentData(float A, float phi, float da, Point ...points) {
  Circle c1 = (Circle) points[0];
  Circle c2 = (Circle) points[1];
  Point t1 = points[2], t2 = points[3], t3 = points[4];
  Point s1 = points[5], s2 = points[6];

  stroke(0, 200, 0);
  fill(0, 200, 0, 20);
  drawPoly(c1, c2, t3);

  stroke(200, 0, 0);
  fill(200, 0, 0, 20);
  drawPoly(c2, t3, t1, t2);

  noFill();
  stroke(0);
  arc(c1.x, c1.y, A/2, A/2, da, phi);

  fill(0);
  text("φ = cos⁻¹(A/H)", c1.x-0, c1.y - 20);
  text("A = r₁ - r₂", c1.x + 7 + (A/2) * cos(phi), c1.y + (A/2) * sin(phi));
  text("H = |c₂ - c₁|", lerp(c1.x, c2.x, 0.5) + 10, lerp(c1.y, c2.y, 0.5) - 10);
  text("O = √(H² - A²)", lerp(t3.x, c2.x, 0.5) - 60, lerp(t3.y, c2.y, 0.5) + 20);
  text("r₂", c2.x + 7 + (c2.r/2) * cos(phi), c2.y + 1 + (c2.r/2) * sin(phi + 0.2));

  noStroke();
  fill(200, 0, 0, 10);
  ellipse(c1.x, c1.y, 2*A, 2*A); 

  stroke(0);
  line(s1.x, s1.y, s2.x, s2.y);
}


void drawIntangentData(float A, float phi, float da, Point ...points) {
  Circle c1 = (Circle) points[0];
  Circle c2 = (Circle) points[1];
  Point t1 = points[2], t2 = points[3], t3 = points[4];
  Point s1 = points[5], s2 = points[6];

  stroke(0, 20);
  noFill();
  ellipse(c2.x, c2.y, 2*A, 2*A);

  stroke(0, 200, 0);
  fill(0, 200, 0, 20);
  drawPoly(c1, c2, t3);

  stroke(200, 0, 0);
  fill(200, 0, 0, 20);
  drawPoly(c1, t3, t2, t1);

  stroke(0);
  arc(c1.x, c1.y, A/2, A/2, da, phi);
  line(s1.x, s1.y, s2.x, s2.y);

  fill(0);
  text("φ = sin⁻¹(O/H)", c1.x-40, c1.y - 5);
  text("r₁", lerp(c1.x, t1.x, 0.5) - 2, lerp(c1.y, t1.y, 0.5) + 10);
  text("H = |c₂ - c₁|", lerp(c1.x, c2.x, 0.5) - 10, lerp(c1.y, c2.y, 0.5));
  text("A = √(H² - O²)", lerp(c1.x, t2.x, 0.5) - 90, lerp(c1.y, t2.y, 0.5) + 50);
  text("O = r₁ + r₂", lerp(c2.x, t2.x, 0.5), lerp(c2.y, t2.y, 0.5) + 20);
}
